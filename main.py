#!/usr/bin/env python3

import os
import sys
from twitter import *
from config import *
import time
import datetime
from db.utils import init_db, insert_tweet, latest_tweet_id_in_db
import logging

base_dir = os.path.abspath(os.path.dirname(__file__))

# set output file for logging
handlers = [logging.FileHandler(base_dir + "log" + LOGGER_FILE_NAME)]
# if get -v or --verbose as argoman print log to console
if "-v" in sys.argv or "--verbose" in sys.argv:
    handlers.append(logging.StreamHandler())
# init logging
logging.basicConfig(level=logging.INFO,
                             format=LOGGER_FORMAT,
                             datefmt=LOGGER_DATE_FORMAT,
                             handlers=handlers
                    )



class GetTweets:

    def __init__(self):
        self.twitter_client = Twitter(
            auth=OAuth(
                ACCESS_TOKEN,
                ACCESS_TOKEN_SECRET,
                API_KEY,
                API_KEY_SECRET
            )
        )

    def get_earliest_tweets(self,
                            count=1,
                            query="freesoftware",
                            until=None):
        """
        return list of tweets in dict format or empty list
        """

        if not until:
            until = datetime.datetime.now() - datetime.timedelta(days=6)
            until = until.strftime("%Y-%m-%d")
        search = self.twitter_client.search.tweets(
            q=query,
            count=count,
            tweet_mode='extended',
            until=until
        )
        return search["statuses"]

    def get_from_last_tweet(self,
                            count=1,
                            query="freesoftware",
                            max_id=None,
                            since_id=None):
        if not since_id:
            # get last id from database
            pass
        search = self.twitter_client.search.tweets(
            q=query,
            count=count,
            tweet_mode='extended',
            since_id=since_id,
            max_id=max_id
        )
        return search["statuses"]


def parse_tweet(tweet):
    result = {
        'tweet_id': 0,
        'created_at': '',
        'text': '',
        'user_id': 0,
        'link_url': [],
        'media_url': [],
        'video_url': []
    }

    # TODO: check if we are catching orginal tweet or RT or quotation
    # chech what to do if it is quote
    # maybe we should have a ignore RT totally
    if tweet['full_text'].startswith('RT'):
        if 'retweeted_status' in tweet:
            parse_tweet(tweet['retweeted_status'])
            pass

    result['tweet_id'] = tweet['id']

    # convert created_at date into timestamp
    result['created_at'] = int(time.mktime(
        time.strptime(tweet['created_at'], "%a %b %d %H:%M:%S +0000 %Y")
    ))
    result['text'] = tweet['full_text']
    result['user_id'] = tweet['user']['id']

    for url_item in tweet['entities']['urls']:
        result['link_url'].append(url_item['expanded_url'])

    if 'media' in tweet['entities']:
        for media_item in tweet['entities']['media']:
            if 'media_url_https' in media_item:
                result['media_url'].append(media_item['media_url_https'])
            else:
                result['media_url'].append(media_item['media_url'])

    if 'extended_entities' in tweet:
        for extended_media_item in tweet['extended_entities']['media']:
            if 'video_info' in extended_media_item:
                result['video_url'].append(
                    extended_media_item['video_info']['variants'][0]['url']
                )
    return result


def process(query="freesoftware",
            count=100,
            max_id=None,
            since_id=None):
    logging.info('########################### Fetching new data ###########################')
    logging.info('since_id : {}'.format(since_id))

    db_file = os.path.join(base_dir, 'db', DB_NAME)
    init_db(db_file, logging)
    get_tweets = GetTweets()
    if since_id is None:
        since_id = latest_tweet_id_in_db(db_file, logging)
    # min_tweet_id will be the max_id for recursive search in this round
    min_tweet_id = 0
    # max_tweet_id will be the since_id for the next round of search
    max_tweet_id = 0
    # zero_tweet_response_count is to measure the number of fail in fetch
    zero_tweet_response_count = 0
    parsed_tweets = []

    # One round of search from current tweet till the since_id
    while zero_tweet_response_count < 3:
        tweets = get_tweets.get_from_last_tweet(since_id=since_id,
                                                max_id=max_id,
                                                count=count,
                                                query=query)

        if len(tweets) == 0:
            zero_tweet_response_count += 1
            logging.info("possibly reached to the end of the search. retry no. {}".format(zero_tweet_response_count))
            time.sleep(6)
            continue

        logging.info("{} tweets from:{}, {}".format(len(tweets), tweets[0]['id'], tweets[-1]['id']))
        # if we have tweet, the previous zero response could be due to some err
        zero_tweet_response_count = 0
        if len(tweets) == 0:
            zero_tweet_response_count += 1

        if max_tweet_id == 0:
            # get the first fetched tweet id since it is the latest one also
            max_tweet_id = tweets[0]['id']

        for tweet in tweets:
            parsed_tweet = parse_tweet(tweet)

            if min_tweet_id == 0:
                # initial value assign
                min_tweet_id = parsed_tweet['tweet_id']

            if parsed_tweet not in parsed_tweets:
                parsed_tweets.append(parsed_tweet)

            min_tweet_id = min(min_tweet_id, parsed_tweet['tweet_id'])

            # store in db
            insert_tweet(parsed_tweet, db_file, logging)

        max_id = min_tweet_id - 1
        logging.info('oldest tweet from: {}'.format(tweets[-1]['created_at']))
        time.sleep(6)

    time.sleep(20)

    process(
        query=query,
        count=count,
        since_id=max_tweet_id,
    )


if __name__ == "__main__":
    process()
