import sqlite3


def create_connection(db_file, logging):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Exception as e:
        logging.error(e)
    return None


def create_table(conn, create_table_sql, logging):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Exception as e:
        logging.error(e)


def init_db(db_file, logging):
    sql_create_tweets_table = """ CREATE TABLE IF NOT EXISTS tweets (
            tweet_id integer PRIMARY KEY,
            user_id integer NOT NULL,
            created_at integer NOT NULL,
            content_text text
        ); """
    sql_create_tweet_entities_table = """CREATE TABLE IF NOT EXISTS tweet_entities (
        id	INTEGER PRIMARY KEY AUTOINCREMENT,
        url	TEXT NOT NULL,
        type_id	INTEGER NOT NULL,
        tweet_id INTEGER NOT NULL,
        download_stage INTEGER DEFAULT 0,
        FOREIGN KEY(tweet_id) REFERENCES tweets(tweet_id)
    );"""
    # create a database connection
    conn = create_connection(db_file, logging)
    with conn:
        create_table(conn, sql_create_tweets_table, logging)
        create_table(conn, sql_create_tweet_entities_table, logging)



def insert_tweet(tweet, db_file, logging):
    conn = create_connection(db_file, logging)

    sql_tweet = ''' INSERT OR IGNORE INTO tweets(tweet_id,user_id,created_at,content_text)
    VALUES(?,?,?,?) '''
    sql_entities = ''' INSERT INTO tweet_entities(url,type_id,tweet_id)
    VALUES(?,?,?) '''

    with conn:
        cur = conn.cursor()

        # Check if the tweet already exist or not
        cur.execute("""SELECT count(`tweet_id`) FROM `tweets`
                    WHERE `tweet_id`=? """, (tweet['tweet_id'],))
        if cur.fetchone()[0] is True:
            logging.warning("This tweet id already exists in db:{}".format(
                tweet['tweet_id'])
                )
        else:
            # the tweet is new

            cur.execute(
                sql_tweet,
                (
                    tweet["tweet_id"],
                    tweet["user_id"],
                    tweet["created_at"],
                    tweet["text"]
                )
            )
            for link in tweet["link_url"]:
                cur.execute(
                    sql_entities,
                    (
                        link,
                        1,
                        tweet["tweet_id"]
                    )
                )
            for media in tweet["media_url"]:
                cur.execute(
                    sql_entities,
                    (
                        media,
                        2,
                        tweet["tweet_id"]
                    )
                )
            for video in tweet["video_url"]:
                cur.execute(
                    sql_entities,
                    (
                        video,
                        3,
                        tweet["tweet_id"]
                    )
                )


def latest_tweet_id_in_db(db_file, logging):

    conn = create_connection(db_file, logging)

    with conn:
        cur = conn.cursor()

        # Check if the tweet already exist or not
        cur.execute("SELECT max(tweet_id) FROM `tweets`")
        result = cur.fetchone()
        if len(result) == 0:
            # there is no tweet in db, so return 1 to get all available tweets
            return 1
        return result[0]
